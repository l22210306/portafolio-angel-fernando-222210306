CREATE DATABASE Certificados;
USE Certificados;

-- Tabla para los empleados 
-- 1
CREATE TABLE Empleados (
    ID INT PRIMARY KEY,
    Nombre VARCHAR(100),
    Departamento VARCHAR(100),
    Puesto VARCHAR(100),
    FechaContratacion DATE,
    Email VARCHAR(255),
    Telefono VARCHAR(20)
);

-- Tabla para las certificaciones
-- 2
CREATE TABLE Certificaciones (
    ID INT PRIMARY KEY,
    Nombre VARCHAR(100),
    Descripcion TEXT,
    FechaVencimiento DATE
);

-- Tabla para los cumplimientos legales
-- 3
CREATE TABLE CumplimientosLegales (
    ID INT PRIMARY KEY,
    Nombre VARCHAR(100),
    Descripcion TEXT,
    FechaVencimiento DATE
);

-- Tabla para las relaciones entre empleados y certificaciones
-- 4
CREATE TABLE Empleados_Certificaciones (
    EmpleadoID INT,
    CertificacionID INT,
    FechaObtencion DATE,
    FOREIGN KEY (EmpleadoID) REFERENCES Empleados(ID),
    FOREIGN KEY (CertificacionID) REFERENCES Certificaciones(ID),
    PRIMARY KEY (EmpleadoID, CertificacionID)
);

-- Tabla para las relaciones entre empleados y cumplimientos legales
-- 5
CREATE TABLE Empleados_CumplimientosLegales (
    EmpleadoID INT,
    CumplimientoLegalID INT,
    FechaCumplimiento DATE,
    FOREIGN KEY (EmpleadoID) REFERENCES Empleados(ID),
    FOREIGN KEY (CumplimientoLegalID) REFERENCES CumplimientosLegales(ID),
    PRIMARY KEY (EmpleadoID, CumplimientoLegalID)
);

-- Tabla para el registro de actualizaciones de certificaciones
-- 6
CREATE TABLE ActualizacionesCertificaciones (
    ID INT PRIMARY KEY,
    EmpleadoID INT,
    CertificacionID INT,
    FechaActualizacion DATE,
    Observaciones TEXT,
    FOREIGN KEY (EmpleadoID) REFERENCES Empleados(ID),
    FOREIGN KEY (CertificacionID) REFERENCES Certificaciones(ID)
);

-- Tabla para el registro de actualizaciones de cumplimientos legales
-- 7
CREATE TABLE ActualizacionesCumplimientosLegales (
    ID INT PRIMARY KEY,
    EmpleadoID INT,
    CumplimientoLegalID INT,
    FechaActualizacion DATE,
    Observaciones TEXT,
    FOREIGN KEY (EmpleadoID) REFERENCES Empleados(ID),
    FOREIGN KEY (CumplimientoLegalID) REFERENCES CumplimientosLegales(ID)
);

-- Tabla para los departamentos
-- 8
CREATE TABLE Departamentos (
    ID INT PRIMARY KEY,
    Nombre VARCHAR(100),
    SupervisorID INT,
    FOREIGN KEY (SupervisorID) REFERENCES Empleados(ID)
);

-- Tabla para los registros de entrenamientos y cursos
-- 9
CREATE TABLE Entrenamientos (
    ID INT PRIMARY KEY,
    Nombre VARCHAR(100),
    Descripcion TEXT
);

-- Tabla para las relaciones entre empleados y entrenamientos
-- 10
CREATE TABLE Empleados_Entrenamientos (
    EmpleadoID INT,
    EntrenamientoID INT,
    FechaRealizacion DATE,
    FOREIGN KEY (EmpleadoID) REFERENCES Empleados(ID),
    FOREIGN KEY (EntrenamientoID) REFERENCES Entrenamientos(ID),
    PRIMARY KEY (EmpleadoID, EntrenamientoID)
);

-- Tabla para los registros de incidentes de seguridad
-- 11
CREATE TABLE IncidentesSeguridad (
    ID INT PRIMARY KEY,
    FechaIncidente DATE,
    Descripcion TEXT,
    EmpleadoID INT,
    FOREIGN KEY (EmpleadoID) REFERENCES Empleados(ID)
);

-- Tabla para los registros de evaluaciones de desempeño
-- 12
CREATE TABLE EvaluacionesDesempeno (
    ID INT PRIMARY KEY,
    EmpleadoID INT,
    FechaEvaluacion DATE,
    Descripcion TEXT,
    FOREIGN KEY (EmpleadoID) REFERENCES Empleados(ID)
);
 
-- Tabla para los registros de auditorías internas
-- 13
CREATE TABLE AuditoriasInternas (
    ID INT PRIMARY KEY,
    FechaAuditoria DATE,
    Descripcion TEXT,
    ResponsableID INT,
    FOREIGN KEY (ResponsableID) REFERENCES Empleados(ID)
);

-- Tabla para los hallazgos de auditorías internas
-- 14
CREATE TABLE HallazgosAuditorias (
    ID INT PRIMARY KEY,
    AuditoriaID INT,
    Descripcion TEXT,
    AccionCorrectiva TEXT,
    FechaResolucion DATE,
    FOREIGN KEY (AuditoriaID) REFERENCES AuditoriasInternas(ID)
);

-- Tabla para los registros de permisos y licencias
-- 15
CREATE TABLE PermisosLicencias (
    ID INT PRIMARY KEY,
    Tipo VARCHAR(100),
    Descripcion TEXT,
    FechaEmision DATE,
    FechaExpiracion DATE
);

-- Tabla de relación entre empleados y permisos/licencias
-- 16
CREATE TABLE Empleados_PermisosLicencias (
    EmpleadoID INT,
    PermisoLicenciaID INT,
    FOREIGN KEY (EmpleadoID) REFERENCES Empleados(ID),
    FOREIGN KEY (PermisoLicenciaID) REFERENCES PermisosLicencias(ID),
    PRIMARY KEY (EmpleadoID, PermisoLicenciaID)
);
